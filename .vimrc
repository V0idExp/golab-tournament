set tw=120
set ts=4 sts=4 sw=4 noexpandtab
colorscheme solarized
set wildignore+=pkg,bin,github.com
