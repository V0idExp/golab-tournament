package tournament

import (
	"appengine"
	"encoding/json"
	"fmt"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"glt/api/session"
	"glt/models"
	"net/http"
	"strconv"
)

const (
	minTournamentNameLen = 3
)

// used for (de)serializing JSON
type tournamentData struct {
	Name     string `json:"name"`
	Finished bool   `json:"finished"`
	ID       int64  `json:"id"`
	IsMember bool   `json:"isMember"`
}

type fixtureData struct {
	Result      string `json:"result"`
	ID          int64  `json:"id"`
	FirstParty  string `json:"firstParty"`
	SecondParty string `json:"secondParty"`
}

func Middleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	// retrieve the tournament id from the request
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// check if the user actually takes part of the tournament
	c := appengine.NewContext(r)
	u := session.GetRequestUser(r)
	_, err = models.GetMembership(c, u.ID(), int64(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	// query the tournament and store it in request context
	t, err := models.GetTournament(c, int64(id))
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	context.Set(r, "tournament", t)

	// go ahead
	next(w, r)
}

func GetRequestTournament(r *http.Request) *models.Tournament {
	if t := context.Get(r, "tournament"); t != nil {
		return t.(*models.Tournament)
	}
	return nil
}

func Create(w http.ResponseWriter, r *http.Request) {
	// decode tournament data
	var data tournamentData
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil ||
		len(data.Name) < minTournamentNameLen {
		http.Error(w, "invalid name", http.StatusBadRequest)
		return
	}

	// create a new tournament
	u := session.GetRequestUser(r)
	c := appengine.NewContext(r)
	_, err := models.NewTournament(c, u, data.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "{}")
}

func List(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := session.GetRequestUser(r)
	if ts, err := models.GetTournaments(c); err == nil {
		if ms, err := models.GetUserMemberships(c, u.ID()); err == nil {
			// create a boolean map from memberships
			mm := make(map[int64]bool)
			for _, m := range ms {
				mm[m.TournamentID] = true
			}

			// populate the result
			data := make([]tournamentData, 0)
			for _, t := range ts {
				_, isMember := mm[t.ID]
				data = append(data, tournamentData{
					Name:     t.Name,
					Finished: t.Finished,
					ID:       t.ID,
					IsMember: isMember,
				})
			}

			// encode it as JSON
			json.NewEncoder(w).Encode(map[string]interface{}{
				"tournaments": data,
			})
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func Fixtures(w http.ResponseWriter, r *http.Request) {
	tid, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	c := appengine.NewContext(r)
	t, err := models.GetTournament(c, int64(tid))
	if err != nil {
		var status int
		if err == models.ErrTournamentNotFound {
			status = http.StatusNotFound
		} else {
			status = http.StatusInternalServerError
		}
		http.Error(w, err.Error(), status)
		return
	}

	fs, err := models.GetTournamentFixtures(c, t.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	data := make([]fixtureData, len(fs))
	for i, f := range fs {
		u1, u1err := models.GetUserByID(c, f.PartyID1)
		u2, u2err := models.GetUserByID(c, f.PartyID2)
		if u1err != nil || u2err != nil {
			http.Error(
				w,
				fmt.Sprintf("failed to fetch users data: %v %v", u1err, u2err),
				http.StatusInternalServerError,
			)
			return
		}
		data[i].ID = f.ID
		switch f.Result {
		case models.FixtureResultVictory:
			data[i].Result = "victory"
		case models.FixtureResultDraw:
			data[i].Result = "draw"
		case models.FixtureResultDefeat:
			data[i].Result = "defeat"
		}
		data[i].FirstParty = u1.Name
		data[i].SecondParty = u2.Name
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"tournament": map[string]interface{}{
			"name":     t.Name,
			"id":       t.ID,
			"finished": t.Finished,
			"fixtures": data,
		},
	})
}

func Join(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// check for the existance of a membership
	c := appengine.NewContext(r)
	u := session.GetRequestUser(r)
	m, _ := models.GetMembership(c, u.ID(), int64(id))
	if m != nil {
		http.Error(w, "user is already member of the tournament", http.StatusBadRequest)
		return
	}

	// lookup the tournament and create a membership for it
	t, err := models.GetTournament(c, int64(id))
	if err != nil {
		var status int
		if err == models.ErrTournamentNotFound {
			status = http.StatusNotFound
		} else {
			status = http.StatusInternalServerError
		}
		http.Error(w, err.Error(), status)
		return
	}

	// create a new membership
	_, err = models.NewMembership(c, u.ID(), t.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "{}")
}

func SubmitResult(w http.ResponseWriter, r *http.Request) {
	// decode submitted fixture data
	var data fixtureData
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		http.Error(w, "bad data", http.StatusBadRequest)
		return
	}
	var result models.FixtureResult
	switch data.Result {
	case "victory":
		result = models.FixtureResultVictory
		break
	case "defeat":
		result = models.FixtureResultDefeat
		break
	case "draw":
		result = models.FixtureResultDraw
		break
	default:
		http.Error(w, "invalid result value", http.StatusBadRequest)
		return
	}

	// extract the id from URL values
	fid, err := strconv.Atoi(mux.Vars(r)["fid"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// update the fixture
	t := GetRequestTournament(r)
	if t == nil {
		// actually we could panic here, since this would be a middleware bug and should *NEVER* happen
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	c := appengine.NewContext(r)
	err = models.UpdateFixture(c, int64(fid), t.ID, result)
	if err != nil {
		var errStatus int
		if err == models.ErrFixtureAlreadySet || err == models.ErrFixtureNotFound {
			errStatus = http.StatusBadRequest
		} else {
			errStatus = http.StatusInternalServerError
		}
		http.Error(w, err.Error(), errStatus)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintf(w, "{}")
}

func UpdateTournaments(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	err := models.UpdateTournaments(c)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	// status OK
}
