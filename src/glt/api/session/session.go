package session

import (
	"appengine"
	"github.com/gorilla/context"
	"glt/models"
	"net/http"
)

func Middleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	// get the session token from request headers
	token := r.Header.Get("X-Session-Token")
	if len(token) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// lookup the session object on datastore by its token
	c := appengine.NewContext(r)
	s, err := models.GetSession(c, token)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// store the session in request context
	context.Set(r, "session", s)

	// advance to next handler
	next(w, r)
}

func GetRequestUser(r *http.Request) *models.User {
	if s := context.Get(r, "session"); s != nil {
		return s.(*models.Session).GetUser()
	}
	return nil
}
