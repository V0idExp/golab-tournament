package user

import (
	"appengine"
	"encoding/json"
	"fmt"
	"glt/models"
	"net/http"
	"regexp"
)

const (
	minPasswdLen = 8
)

var (
	usernamePattern = regexp.MustCompile(`[A-z0-9_]{3,}`)
	emailPattern    = regexp.MustCompile(`[A-z][\w-]{2,}@\w{3,}\.[a-z]{2,4}`)
)

type credentials struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

func Register(w http.ResponseWriter, r *http.Request) {
	u := new(models.User)
	c := appengine.NewContext(r)

	// decode data into a User struct
	if err := json.NewDecoder(r.Body).Decode(u); err != nil {
		http.Error(w, "bad data", http.StatusBadRequest)
		return
	} else if !usernamePattern.MatchString(u.Name) {
		http.Error(w, "invalid name", http.StatusBadRequest)
		return
	} else if len(u.Password) < minPasswdLen {
		http.Error(w, "password too short", http.StatusBadRequest)
		return
	} else if !emailPattern.MatchString(u.Email) {
		http.Error(w, "invalid email", http.StatusBadRequest)
		return
	}

	// check for the existance of the user with same credentials
	if eu, _ := models.GetUserByName(c, u.Name); eu != nil {
		http.Error(w, "user with given name already exists", http.StatusBadRequest)
		return
	} else if eu, _ := models.GetUserByEmail(c, u.Email); eu != nil {
		http.Error(w, "user with given email address already exists", http.StatusBadRequest)
		return
	}

	// save the User struct on datastore
	if err := u.Put(c); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "{}")
}

func Login(w http.ResponseWriter, r *http.Request) {
	var cr credentials

	// validate credentials
	if err := json.NewDecoder(r.Body).Decode(&cr); err != nil ||
		!usernamePattern.MatchString(cr.Name) ||
		len(cr.Password) < minPasswdLen {
		http.Error(w, "bad data", http.StatusBadRequest)
		return
	}

	// attempt to fetch the user from datastore and check credentials;
	// on success create a new session and return the token
	c := appengine.NewContext(r)
	if u, err := models.GetUserByName(c, cr.Name); err == nil && u != nil && u.Password == cr.Password {
		if s, err := models.NewSession(c, u); err == nil {
			// return the session token
			json.NewEncoder(w).Encode(&s)
		} else {
			http.Error(w, fmt.Sprintf("%v", err), http.StatusUnauthorized)
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
	}
}
