package models

import (
	"appengine"
	"appengine/datastore"
	"appengine/delay"
	"fmt"
)

type Tournament struct {
	Name     string
	Finished bool
	ID       int64
}

type Membership struct {
	TournamentID int64
	UserID       int64
}

type Fixture struct {
	ID           int64
	TournamentID int64
	PartyID1     int64
	PartyID2     int64
	Result       FixtureResult
}

type FixtureResult int

var (
	ErrTournamentNotFound = fmt.Errorf("tournament not found")
	ErrMembershipNotFound = fmt.Errorf("membership not found")
	ErrFixtureNotFound    = fmt.Errorf("fixture not found")
	ErrFixtureAlreadySet  = fmt.Errorf("fixture result is already set")
)

type filter struct {
	Key   string
	Op    string
	Value interface{}
}

const (
	FixtureResultNone FixtureResult = iota
	FixtureResultVictory
	FixtureResultDefeat
	FixtureResultDraw
	tournamentKind = "tournament"
	membershipKind = "membership"
	fixtureKind    = "fixture"
)

func NewTournament(c appengine.Context, u *User, name string) (*Tournament, error) {
	t := Tournament{
		Name:     name,
		Finished: false,
	}

	// allocate a new tournament identifier
	id, _, err := datastore.AllocateIDs(c, tournamentKind, nil, 1)
	if err != nil {
		return nil, err
	}
	t.ID = id

	// make a key from it and store the tournament object instance using that key
	key := datastore.NewKey(c, tournamentKind, "", t.ID, nil)
	_, err = datastore.Put(c, key, &t)
	if err != nil {
		return nil, fmt.Errorf("failed to create new tournament: %v", err)
	}

	// create a membership for the creator
	_, err = NewMembership(c, u.ID(), t.ID)
	if err != nil {
		// delete the tournament on failure
		datastore.Delete(c, key)
		return nil, err
	}
	return &t, nil
}

func GetTournaments(c appengine.Context) ([]*Tournament, error) {
	ts := make([]*Tournament, 0)
	q := datastore.NewQuery(tournamentKind).Order("-Name")
	if _, err := q.GetAll(c, &ts); err != nil {
		return nil, fmt.Errorf("failed to retrieve tournaments: %v", err)
	}
	return ts, nil
}

func GetTournament(c appengine.Context, id int64) (*Tournament, error) {
	var t Tournament
	key := datastore.NewKey(c, tournamentKind, "", id, nil)
	if err := datastore.Get(c, key, &t); err != nil {
		if err == datastore.ErrNoSuchEntity {
			return nil, ErrTournamentNotFound
		} else {
			return nil, fmt.Errorf("failed to retrieve tournament %d: %v", id, err)
		}
	}
	return &t, nil
}

func (t *Tournament) Put(c appengine.Context) error {
	k := datastore.NewKey(c, tournamentKind, "", t.ID, nil)
	if _, err := datastore.Put(c, k, t); err != nil {
		return err
	}
	return nil
}

func UpdateTournaments(c appengine.Context) error {
	q := datastore.NewQuery(tournamentKind).Filter("Finished =", false).KeysOnly()
	if keys, err := q.GetAll(c, nil); err != nil {
		return err
	} else {
		// create a task function
		taskFunc := delay.Func("tournament-update", func(c appengine.Context, tid int64) error {
			// fetch the whole tournament object
			t, err := GetTournament(c, tid)
			if err != nil {
				return err
			}

			// get its fixtures and check all results
			fixtures, err := GetTournamentFixtures(c, t.ID)
			if err != nil {
				return err
			}
			finished := true
			for _, f := range fixtures {
				if f.Result == FixtureResultNone {
					finished = false
					break
				}
			}

			// if all fixtures have the results set, update the tournament
			if finished {
				t.Finished = true
				err = t.Put(c)
				if err != nil {
					return err
				}
			}
			return nil
		})

		// spawn it for each tournament; in case spawn rate exceeds the configured limits,
		// the next time the cron task is invoked, the remaining tournaments will be processed
		for _, k := range keys {
			taskFunc.Call(c, k.IntID())
		}
	}
	return nil
}

func queryMemberships(c appengine.Context, filters []filter) ([]*Membership, error) {
	ms := make([]*Membership, 0)
	q := datastore.NewQuery(membershipKind)
	for _, f := range filters {
		q = q.Filter(fmt.Sprintf("%s %s", f.Key, f.Op), f.Value)
	}
	if _, err := q.GetAll(c, &ms); err != nil {
		return nil, fmt.Errorf("failed to retrieve memberships: %v", err)
	}
	return ms, nil
}

func NewMembership(c appengine.Context, uid, tid int64) (m *Membership, err error) {
	// query all currrent memberships in the tournament
	current, err := queryMemberships(c, []filter{{"TournamentID", "=", tid}})
	if err != nil {
		return nil, err
	}

	// attempt to create and put new membership object on datastore
	m = &Membership{
		TournamentID: tid,
		UserID:       uid,
	}
	key := datastore.NewIncompleteKey(c, membershipKind, nil)
	if key, err = datastore.Put(c, key, m); err != nil {
		return nil, fmt.Errorf("failed to create a tournament membership")
	}

	// in case of panic, make sure we've deleted the just-created membership
	defer func() {
		if pval := recover(); pval != nil {
			datastore.Delete(c, key)
			m = nil
			err = pval.(error)
		}
	}()

	if len(current) > 0 {
		// allocate `num_of_memberships * 2` fixture identifiers and create them in batch
		firstId, _, _ := datastore.AllocateIDs(c, fixtureKind, nil, len(current)*2)

		// this could be done with PutMulti(), but for demonstrational purposes, we're using the transaction mechanism
		err = datastore.RunInTransaction(c, func(c appengine.Context) error {
			for i := 0; i < len(current); i++ {
				var err error
				// create the going fixture
				_, err = newFixture(c, firstId+int64(i*2), uid, current[i].UserID, tid)
				if err != nil {
					return err
				}
				// create the return fixture
				_, err = newFixture(c, firstId+int64(i*2+1), current[i].UserID, uid, tid)
				if err != nil {
					return err
				}
			}
			return nil
		}, nil)
		if err != nil {
			panic(err)
		}
	}
	return m, nil
}

func GetUserMemberships(c appengine.Context, uid int64) ([]*Membership, error) {
	ms, err := queryMemberships(c, []filter{{"UserID", "=", uid}})
	return ms, err
}

func GetTournamentMemberships(c appengine.Context, tid int64) ([]*Membership, error) {
	ms, err := queryMemberships(c, []filter{{"TournamentID", "=", tid}})
	return ms, err
}

func GetMembership(c appengine.Context, uid, tid int64) (*Membership, error) {
	ms, err := queryMemberships(
		c,
		[]filter{{
			"TournamentID", "=", tid,
		}, {
			"UserID", "=", uid,
		}},
	)
	if err != nil {
		return nil, err
	} else if len(ms) == 0 {
		return nil, ErrMembershipNotFound
	}
	return ms[0], nil
}

func newFixture(c appengine.Context, fid, pid1, pid2, tid int64) (*Fixture, error) {
	f := Fixture{
		ID:           fid,
		TournamentID: tid,
		PartyID1:     pid1,
		PartyID2:     pid2,
		Result:       FixtureResultNone,
	}
	// create a parent key for the given tournament fixtures
	pkey := datastore.NewKey(c, fixtureKind, "", tid, nil)
	if _, err := datastore.Put(c, datastore.NewKey(c, fixtureKind, "", fid, pkey), &f); err != nil {
		return nil, fmt.Errorf("failed to create fixture: %v", err)
	}
	return &f, nil
}

func UpdateFixture(c appengine.Context, fid, tid int64, r FixtureResult) error {
	pkey := datastore.NewKey(c, fixtureKind, "", tid, nil)
	key := datastore.NewKey(c, fixtureKind, "", fid, pkey)
	var f Fixture

	// fetch the fixture
	if err := datastore.Get(c, key, &f); err != nil {
		if err == datastore.ErrNoSuchEntity {
			return ErrFixtureNotFound
		} else {
			return fmt.Errorf("failed to retrieve fixture %d: %v", fid, err)
		}
	}

	// check whether the fixture was already updated with a result
	if f.Result != FixtureResultNone {
		return ErrFixtureAlreadySet
	}

	// update the result and put it back
	f.Result = r
	if _, err := datastore.Put(c, key, &f); err != nil {
		return fmt.Errorf("failed to update fixture %d: %v", fid, err)
	}
	return nil
}

func GetTournamentFixtures(c appengine.Context, tid int64) ([]*Fixture, error) {
	fs := make([]*Fixture, 0)
	q := datastore.NewQuery(fixtureKind).Filter("TournamentID =", tid)
	if _, err := q.GetAll(c, &fs); err != nil {
		return nil, err
	}
	return fs, nil
}
