package models

import (
	"appengine"
	"appengine/datastore"
	"fmt"
)

type User struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	id       int64
}

var (
	ErrUserNotFound = fmt.Errorf("user not found")
)

const (
	userKind = "user"
)

func (u *User) Put(c appengine.Context) (err error) {
	var key *datastore.Key

	// for new user instances, allocate a new identifier
	if u.id == 0 {
		u.id, _, err = datastore.AllocateIDs(c, userKind, nil, 1)
		if err != nil {
			return err
		}
	}

	// put (ur update) the instance on datasture
	key = datastore.NewKey(c, userKind, "", u.id, nil)
	_, err = datastore.Put(c, key, u)
	if err != nil {
		return fmt.Errorf("failed to put user %d on datastore", u.id)
	}
	return nil
}

func (u *User) ID() int64 {
	return u.id
}

func getUserBy(c appengine.Context, filters map[string]interface{}) (*User, error) {
	var u []*User
	q := datastore.NewQuery(userKind)
	for k, v := range filters {
		q = q.Filter(fmt.Sprintf("%s =", k), v)
	}
	q = q.Limit(1)
	if keys, err := q.GetAll(c, &u); err == nil && len(u) == 1 {
		u[0].id = keys[0].IntID()
		return u[0], nil
	} else if err == datastore.ErrNoSuchEntity {
		return nil, ErrUserNotFound
	} else {
		return nil, err
	}
}

func GetUserByName(c appengine.Context, name string) (*User, error) {
	filters := map[string]interface{}{
		"Name": name,
	}
	u, err := getUserBy(c, filters)
	return u, err
}

func GetUserByEmail(c appengine.Context, email string) (*User, error) {
	filters := map[string]interface{}{
		"Email": email,
	}
	u, err := getUserBy(c, filters)
	return u, err
}

func GetUserByID(c appengine.Context, id int64) (*User, error) {
	var u User
	key := datastore.NewKey(c, userKind, "", id, nil)
	if err := datastore.Get(c, key, &u); err != nil {
		return nil, fmt.Errorf("failed to fetch the user %d from datastore", id)
	}
	u.id = id
	return &u, nil
}
