package models

import (
	"appengine"
	"appengine/datastore"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
)

type Session struct {
	Token  string `json:"token"`
	UserID int64  `json:"userId"`
	user   *User
}

var (
	ErrSessionNotFound = fmt.Errorf("session not found")
)

const (
	sessionKind = "session"
)

func genToken() (string, error) {
	// allocate a buffer and fill it with some random data
	buffer := make([]byte, 1024)
	if _, err := rand.Reader.Read(buffer); err != nil {
		return "", err
	}

	// return a SHA256 hash as hex string
	return fmt.Sprintf("%x", sha256.Sum256(buffer)), nil
}

func NewSession(c appengine.Context, u *User) (*Session, error) {
	// generate the session token
	token, err := genToken()
	if err != nil {
		return nil, fmt.Errorf("failed to generate session token")
	}

	// put the session on datastore
	s := Session{
		Token:  token,
		UserID: u.ID(),
		user:   u,
	}
	_, err = datastore.Put(c, datastore.NewKey(c, sessionKind, token, 0, nil), &s)
	if err != nil {
		return nil, fmt.Errorf("session creation failed")
	}
	return &s, nil
}

func GetSession(c appengine.Context, token string) (*Session, error) {
	var s Session
	key := datastore.NewKey(c, sessionKind, token, 0, nil)
	if err := datastore.Get(c, key, &s); err != nil {
		if err == datastore.ErrNoSuchEntity {
			return nil, ErrSessionNotFound
		} else {
			return nil, fmt.Errorf("failed to retrieve session: %v", err)
		}
	}

	// query the user by the id stored in session object
	u, err := GetUserByID(c, s.UserID)
	if err != nil {
		return nil, err
	}
	s.user = u
	return &s, nil
}

func (s *Session) GetUser() *User {
	return s.user
}
