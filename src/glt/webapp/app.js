/**
 * Ember app initialization.
 */
Ember.Application.initializer({
    name: 'session',

    initialize: function(container, application) {
        App.Session = Ember.Object.extend({
            init: function() {
                this._super();
                this.set('authToken', $.cookie('auth_token'));
                this.set('authUserId', $.cookie('auth_user'));
            },

            authTokenChanged: function() {
                $.cookie('auth_token', this.get('authToken'));
            }.observes('authToken'),

            authUserIdChanged: function() {
                var authUserId = this.get('authUserId');
                $.cookie('auth_user', authUserId);
                if (!Ember.isEmpty(authUserId)) {
                    //this.set('authUser', App.User.find(authUserId));
                }
            }.observes('authUserId')
        }).create();
    }
});
window.App = Ember.Application.create();

App.Router.reopen({
	rootURL: '/web',
})

/**
 * Ember-data RESTful adapter configuration.
 */
App.ApplicationAdapter = DS.RESTAdapter.extend({
    host: 'http://localhost:8080',
    headers: function() {
        return {
            'X-Session-Token': App.Session.authToken
        };
    }.property('App.Session.authToken')
});

/**
 * Basic route with access restricted to authenticated users.
 */
AuthenticatedRoute = Ember.Route.extend({
    redirectToLogin: function(transition) {
        App.Session.set('attemptedTransition', transition);
        this.transitionTo('registration');
    },

    beforeModel: function(transition) {
        if (!App.Session.get('authToken')) {
            this.redirectToLogin(transition);
        }
    }
});

/**
 * Routes.
 */
App.Router.map(function() {
    this.route('registration');
    this.resource('tournaments', function(){
        this.route('tournament', { path: '/:tournament_id' });
    });
});


/**
 * Models
 */
App.Tournament = DS.Model.extend({
    name: DS.attr('string'),
    finished: DS.attr('boolean'),
    isMember: DS.attr('boolean'),
});

App.Fixture = DS.Model.extend({
    result: DS.attr('string'),
    firstParty: DS.attr('boolean'),
    secondParty: DS.attr('boolean'),
});

/**
 * Index (default) route override - we want it to be accessibile only by
 * authenticated users.
 */
App.IndexRoute = AuthenticatedRoute.extend({
	model: function() {
		return this.store.findAll('tournament');
	},
    actions: {
        joinTournament: function(t) {
            $btn = $('#join-' + t.id);
            var route = this;

            // set the button to disabled and change label
            var oldText = $btn.text();
            function resetButton() {
                $btn.text(oldText);
                $btn.removeClass('disabled');
            }
            $btn.text('Joining...');
            $btn.addClass('disabled');

            // make a request to the API
            var host = this.store.adapterFor('application').get('host');
            $.ajax({
                type: 'POST',
                url: host + '/tournaments/join/' + t.id,
                headers: {
                    'X-Session-Token': App.Session.get('authToken'),
                },
            }).success(function() {
                resetButton();
                // there's probably a better way to do this with ember-data
                // no time to find it >.<
                location.reload();
            });
        },
        viewTournament: function(t) {
            this.transitionTo('tournaments.tournament', t);
        }
    }
});

/**
 * Registration route.
 */
App.RegistrationRoute = Ember.Route.extend({
    beforeModel: function(transition) {
        if (App.Session.get('authUserId')) {
            this.transitionTo('index');
        }
    }
});

/**
 * Registration controller.
 */
App.RegistrationController = Ember.Controller.extend({
    actions: {
        login: function() {
            var self = this;
            var data = this.getProperties('username', 'password');
            if (!Ember.isEmpty(data.username) && !Ember.isEmpty(data.password)) {
                var host = this.store.adapterFor('application').get('host');
                var postData = {
                    name:     data.username,
                    password: data.password
                };
                $.post(host + '/user/login', JSON.stringify(postData), null, 'json')
                .done(function(response) {
                    App.Session.setProperties({
                        authToken:  response.token,
                        authUserId: response.userId,
                    });

                    var attemptedTransition = App.Session.get('attemptedTransition');
                    if (attemptedTransition) {
                        attemptedTransition.retry();
                        App.Session.set('attemptedTransition', null);
                    } else {
                        self.transitionToRoute('index');
                    }
                });
            }
        },

        signup: function() {
            var names = [
                'username',
                'email',
                'password',
                'passwordRep'
            ];
            var emailRe = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var data = {};
            var errors = [];
            this.set('registrationNotif', null);

            // set the button state as active disabled
            var $button = $('#signup-form button[type="submit"]');
            $button.attr('disabled', 'disabled');
            $button.text('signing up...');

            function resetButton() {
                $button.removeAttr('disabled');
                $button.text('sign up');
            }

            function resetFields() {
                names.forEach(function(n) {
                    $('#signup-form input[name="' + n + '"]').val('');
                });
            }

            // fetch field names
            for (var i = 0; i < names.length; i++) {
                data[names[i]] = $('#signup-form input[name="' + names[i] + '"]').val()
            }

            // validate username
            if (Ember.isEmpty(data.username) || data.username.trim().length < 3)
                errors.push("username must be at least 3 characters");

            // validate email
            if (Ember.isEmpty(data.email) || !emailRe.test(data.email))
                errors.push("invalid email address");

            // validate password
            if (Ember.isEmpty(data.password) || data.password.trim().length < 8)
                errors.push("password cannot be shorter than 8 characters");
            else if (data.password != data.passwordRep)
                errors.push("passwords don't match");

            if (errors.length) {
                this.set('registrationErrors', errors);
                resetButton();
            }
            else {
                var postData = {
                    name:     data.username,
                    email:    data.email,
                    password: data.password,
                };
                var host = this.store.adapterFor('application').get('host');
                var self = this;

                $.post(host + '/user', JSON.stringify(postData), null, 'json')
                .done(function() {
                    self.set('registrationErrors', null);

                    function attemptLogin(retries) {
                        // attempt login with the just created user
                        var loginPostData = {
                            name:     data.username,
                            password: data.password
                        };
                        $.ajax({
                            type: 'POST',
                            url:  host + '/user/login',
                            data: JSON.stringify(loginPostData),
                            dataType: 'json',
                            success: function(response) {
                                resetButton();
                                resetFields();

                                App.Session.setProperties({
                                    authToken:  response.token,
                                    authUserId: response.userId,
                                });

                                var attemptedTransition = App.Session.get('attemptedTransition');
                                if (attemptedTransition) {
                                    attemptedTransition.retry();
                                    App.Session.set('attemptedTransition', null);
                                } else {
                                    self.transitionToRoute('index');
                                }
                            },
                            error: function(response) {
                                if (--retries) {
                                    window.setTimeout(attemptLogin, 800, retries);
                                }
                                else {
                                    self.set(
                                        'registrationNotif',
                                        'Registration succeeded, now you can login using your credentials!'
                                    );
                                    resetButton();
                                    resetFields();
                                }
                            }
                        })
                    }
                    window.setTimeout(attemptLogin, 800, 5);
                })
                .fail(function(response) {
                    self.set('registrationErrors', [response.responseText]);
                    resetButton();
                });
            }
        }
    }
});
