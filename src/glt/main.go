package glt

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"glt/api/session"
	"glt/api/tournament"
	"glt/api/user"
	"net/http"
)

func createRouter() *mux.Router {
	// root "public", router
	router := mux.NewRouter()

	// GET "/tasks/update_tournaments" - endpoint called by cron periodically
	router.HandleFunc("/tasks/update_tournaments", tournament.UpdateTournaments)

	// POST "/user" - register new user
	router.Handle(
		"/user",
		http.HandlerFunc(user.Register),
	).Methods("POST")

	// POST "/login" - log in with a registered user
	router.Handle(
		"/user/login",
		http.HandlerFunc(user.Login),
	).Methods("POST")

	// subrouter with user injection layer
	tournamentRoutes := mux.NewRouter()
	router.PathPrefix("/tournaments").Handler(negroni.New(
		negroni.HandlerFunc(session.Middleware),
		negroni.Wrap(tournamentRoutes),
	))

	// GET "/tournament" - retrieve the list of tournaments
	tournamentRoutes.HandleFunc(
		"/tournaments",
		tournament.List,
	).Methods("GET")

	// POST "/tournaments" - create a new tournament
	tournamentRoutes.HandleFunc(
		"/tournaments",
		tournament.Create,
	).Methods("POST")

	// POST "/tournament/join/{id}" - join a tournament
	tournamentRoutes.HandleFunc(
		"/tournaments/join/{id:[0-9]+}",
		tournament.Join,
	).Methods("POST")

	// subrouter with tournament injection layer
	tournamentMgmtRoutes := mux.NewRouter()
	tournamentRoutes.PathPrefix("/tournaments/{id:[0-9]+}").Handler(negroni.New(
		negroni.HandlerFunc(tournament.Middleware),
		negroni.Wrap(tournamentMgmtRoutes),
	))

	tournamentMgmtRoutes.HandleFunc(
		"/tournaments/{id:[0-9]+}",
		tournament.Fixtures,
	).Methods("GET")

	// PUT "/tournament/{id}/fixture/{fid}" - submit fixture result
	tournamentMgmtRoutes.HandleFunc(
		"/tournaments/{id:[0-9]+}/fixture/{fid:[0-9]+}",
		tournament.SubmitResult,
	).Methods("PUT")

	// serve static files
	staticFilesHandler := negroni.NewStatic(http.Dir("./src/glt/webapp"))
	staticFilesHandler.Prefix = "/webapp"
	router.PathPrefix("/webapp").Handler(negroni.New(staticFilesHandler))

	return router
}

func init() {
	router := createRouter()
	app := negroni.Classic()
	app.UseHandler(router)
	http.Handle("/", app)
}
